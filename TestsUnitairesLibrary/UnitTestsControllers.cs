using Micro_API.Controllers;
using Micro_API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;


namespace TestsUnitairesLibrary
{
    [TestClass]
    public class UnitTestsControllers
    {

        [TestMethod]
        public void TestGet()
        {
            //Arrange
            GamesController gController = new GamesController();

            // Act
            var game = gController.GetGame(3); //3 => ID valeur de test


            // Assert
            Assert.IsNotNull(game);
        }

        [TestMethod]
        public void TestPost()
        {
            //Arrange
            GamesController gController = new GamesController();

            // Act
            var Game = new Game {Titre = "J'adore", Genre = "le", Collector = "C#", Quantite = 12 };
            var game = gController.PutGame(42,Game);

            // Assert
            Assert.IsNotNull(game);
        }

        [TestMethod]
        public void TestPut()
        {
            //Arrange
            GamesController gController = new GamesController();

            // Act
            var Game = new Game { Titre = "En fait je", Genre = "suis pas", Collector = "si s�r", Quantite = 12 };
            var game = gController.PutGame(42, Game);

            // Assert
            Assert.IsNotNull(game);
        }


        [TestMethod]
        public void TestDelete()
        {
            //Arrange
            GamesController gController = new GamesController();

            // Act
            //var Game = new Game {};
            var game = gController.DeleteGame(42);

            // Assert
            Assert.IsNotNull(game);
        }
    }
}
