using Micro_API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestsUnitairesLibrary
{
    [TestClass]
    public class UnitTestsModels
    {
        [TestMethod]
        public void TestGameModel()
        {
            //Arrange
            var game = new Game { Titre = "Billy", Catégorie = "GOODIES", Quantite = 2, Collector = "Y" };

            // Act

            // Assert
            Assert.IsNotNull(game);
        }
    }
}
