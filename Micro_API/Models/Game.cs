﻿using System;
using System.Collections.Generic;

namespace Micro_API.Models
{
    public partial class Game
    {
        public int Id { get; set; }
        public string Titre { get; set; }
        public string Genre { get; set; }
        public string Catégorie { get; set; }
        public string Stock { get; set; }
        public string Prix { get; set; }
        public string Collector { get; set; }
        public string Editeur { get; set; }
        public int? Quantite { get; set; }
    }
}
