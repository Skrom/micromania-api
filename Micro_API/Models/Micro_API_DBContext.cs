﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Micro_API.Models
{
    public partial class GameController : DbContext
    {
        //public Micro_API_DBContext()
        //{
        //}

        public GameController(DbContextOptions<GameController> options)
            : base(options)
        {
        }

        public virtual DbSet<Game> Game { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer("Server=UC-REKKORT\\SQLEXPRESS;Database=Micro_API_DB;Integrated Security=True");
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Catégorie)
                    .HasColumnName("catégorie")
                    .HasMaxLength(50);

                entity.Property(e => e.Collector)
                    .HasColumnName("collector")
                    .HasMaxLength(50);

                entity.Property(e => e.Editeur)
                    .HasColumnName("editeur")
                    .HasMaxLength(50);

                entity.Property(e => e.Genre)
                    .HasColumnName("genre")
                    .HasMaxLength(50);

                entity.Property(e => e.Prix)
                    .HasColumnName("prix")
                    .HasMaxLength(50);

                entity.Property(e => e.Quantite).HasColumnName("quantite");

                entity.Property(e => e.Stock)
                    .HasColumnName("stock")
                    .HasMaxLength(50);

                entity.Property(e => e.Titre)
                    .HasColumnName("titre")
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
